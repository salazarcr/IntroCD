import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import datetime

from matplotlib import dates

pd.options.display.max_columns = None
pd.options.display.max_rows = None

data2021 = pd.read_csv('dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data2020 = pd.read_csv('dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])

data20_21 = data2020
data20_21.append(data2021, ignore_index=True)

dfUTI20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3) & (data20_21['UTI'] == 1)]
dfUTI20_21.dropna(subset=['DT_ENTUTI'], inplace=True)

filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '05/08/4020']
dfUTI20_21.drop(filtro.index, inplace=True)

filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '19/11/2929']
dfUTI20_21.drop(filtro.index, inplace=True)

dfUTI20_21['DT_NOTIFIC'] = pd.to_datetime(dfUTI20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfUTI20_21['DT_NASC'] = pd.to_datetime(dfUTI20_21['DT_NASC'], format='%d/%m/%Y')
dfUTI20_21['DT_ENTUTI'] = pd.to_datetime(dfUTI20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfUTI20_21['DT_SAIDUTI'] = pd.to_datetime(dfUTI20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfUTI20_21[dfUTI20_21.DT_ENTUTI.dt.year > 2021]
dfUTI20_21.drop(filtro.index, inplace=True)

filtro = dfUTI20_21[(dfUTI20_21.DT_ENTUTI.dt.year == 2021) & (dfUTI20_21.DT_ENTUTI.dt.month > 4)]
dfUTI20_21.drop(filtro.index, inplace=True)


filtro = dfUTI20_21[dfUTI20_21.DT_NOTIFIC.dt.year > 2021]
dfUTI20_21.drop(filtro.index, inplace=True)

dfUTI20_21.sort_values(by=['DT_NOTIFIC'])

dfUTI20_21.reset_index(inplace=True)

# dfGroup = dfUTI20_21[['NU_IDADE_N' , 'DT_ENTUTI']].groupby(by='DT_ENTUTI').mean()
# dfGroup.reset_index(inplace=True)

# # print(dfGroup.sort_values(by=['DT_ENTUTI']))

# fig = plt.subplots(figsize=(20, 10))
# plt.scatter(dfGroup['DT_ENTUTI'], dfGroup['NU_IDADE_N'])

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
dfCount = dfGeral20_21[['DT_NOTIFIC' , 'CS_SEXO']].groupby(by='DT_NOTIFIC').count()

# print(dfCount.head())

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from matplotlib import dates

# print(__doc__)

X = dfUTI20_21[['DT_ENTUTI' , 'NU_IDADE_N']]
X['DT_ENTUTI'] = dates.date2num(X['DT_ENTUTI'])
X = X.to_numpy()

range_n_clusters = [9]

for n_clusters in range_n_clusters:
    # Create a subplot with 1 row and 2 columns
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(X)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.nipy_spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper), 0, ith_cluster_silhouette_values, facecolor=color, edgecolor=color, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[: , 1], marker='.', s=30, lw=0, alpha=0.7, c=colors, edgecolor='k')
    # ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7, c=colors)

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1], marker='o', c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers): ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1, s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data " "with n_clusters = %d" % n_clusters), fontsize=14, fontweight='bold')

plt.show()

