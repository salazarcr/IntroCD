import matplotlib.pyplot    as plt
import pandas               as pd
import numpy                as np
import statsmodels.api      as sm

import datetime

from matplotlib                 import dates
from sklearn.model_selection    import train_test_split
from sklearn.metrics            import mean_squared_error, r2_score
from sklearn                    import linear_model

# --------------------------------------------------------------------------------------#
pd.options.display.max_columns = None
pd.options.display.max_rows = None
data2021 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'], low_memory=False)
data2020 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'], low_memory=False)
data20_21 = data2020
data20_21.append(data2021, ignore_index=True)
# --------------------------------------------------------------------------------------#

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '05/08/4020']
dfGeral20_21 = dfGeral20_21.drop(filtro.index)
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '19/11/2929']
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

dfGeral20_21['DT_NOTIFIC'] = pd.to_datetime(dfGeral20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfGeral20_21['DT_SIN_PRI'] = pd.to_datetime(dfGeral20_21['DT_SIN_PRI'], format='%d/%m/%Y')
dfGeral20_21['DT_NASC'] = pd.to_datetime(dfGeral20_21['DT_NASC'], format='%d/%m/%Y')
dfGeral20_21['DT_ENTUTI'] = pd.to_datetime(dfGeral20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfGeral20_21['DT_SAIDUTI'] = pd.to_datetime(dfGeral20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfGeral20_21[dfGeral20_21.DT_NOTIFIC.dt.year > 2021]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2021) & (dfGeral20_21.DT_NOTIFIC.dt.month > 4)]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2020) & (dfGeral20_21.DT_NOTIFIC.dt.month < 4)]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)
# --------------------------------------------------------------------------------------#

# dfCuritiba = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'CURITIBA']
# dfPortoAlegre = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO ALEGRE']
# dfSaoPaulo = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SAO PAULO']
# dfBeloHorizonte = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELO HORIZONTE']
# dfManaus = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'MANAUS']
# dfBelem = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELEM']
# dfPortoVelho = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO VELHO']
# dfSalvador = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SALVADOR']
# dfRecife = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'RECIFE']
dfFortaleza = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'FORTALEZA']

#############################################################################################################################

dfTotalCasos = dfFortaleza[['SEM_PRI', 'DT_NOTIFIC']].groupby(by='SEM_PRI').count()
dfTotalCasos.reset_index(inplace=True)
dfTotalCasos.columns = ['SEM_PRI', 'CASOS']

inicio = dfTotalCasos['SEM_PRI'].index[dfTotalCasos['SEM_PRI'] == 35]
inicio = inicio.tolist()
inicio = inicio[0]
fim = dfTotalCasos.shape[0]

derivadas = pd.DataFrame(index=range(inicio, fim), columns=['sem_epi', 'casos', 1, 2, 3, 4, 5, 6])

for i in derivadas.index:
    derivadas['sem_epi'][i] = dfTotalCasos['SEM_PRI'][i]
    derivadas['casos'][i] = dfTotalCasos['CASOS'][i]
    if i <= fim-4:
        for j in range(1,4):
            derivadas[j][i] = dfTotalCasos['CASOS'][i-j] / dfTotalCasos['CASOS'][i]
            derivadas[3+j][i] = dfTotalCasos['CASOS'][i+j] / dfTotalCasos['CASOS'][i]
    if i == fim-3:
        for j in range(1,4):
            derivadas[j][i] = dfTotalCasos['CASOS'][i-j] / dfTotalCasos['CASOS'][i]
        for j in range(1,3):
            derivadas[3+j][i] = dfTotalCasos['CASOS'][i+j] / dfTotalCasos['CASOS'][i]
    if i == fim-2:
        for j in range(1,4):
            derivadas[j][i] = dfTotalCasos['CASOS'][i-j] / dfTotalCasos['CASOS'][i]
        for j in range(1,2):
            derivadas[3+j][i] = dfTotalCasos['CASOS'][i+j] / dfTotalCasos['CASOS'][i]
    if i == fim-1:
        for j in range(1,4):
            derivadas[j][i] = dfTotalCasos['CASOS'][i-j] / dfTotalCasos['CASOS'][i]
        for j in range(1,1):
            derivadas[3+j][i] = dfTotalCasos['CASOS'][i+j] / dfTotalCasos['CASOS'][i]

derivadas.dropna(inplace=True)

print(derivadas) 

ondaInicio = 0

for i in derivadas.index:
    if (derivadas[1][i] >= 1 and derivadas[2][i] >= 0.8 and derivadas[3][i] >= 0.8 and derivadas[4][i] >= 1 and derivadas[5][i] >= 1 and derivadas[6][i] >= 1.5):
        ondaInicio = derivadas['sem_epi'][i]
        break

if ondaInicio == 0:
    ondaInicio = 44
    print('Controle onda.')

print(ondaInicio)
#############################################################################################################################

# dfCuritiba_FW = dfCuritiba[dfCuritiba['SEM_PRI'] <= 44]
# dfPortoAlegre_FW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] <= 44]
# dfSaoPaulo_FW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] <= 44]
# dfBeloHorizonte_FW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] <= 44]
# dfManaus_FW = dfManaus[dfManaus['SEM_PRI'] <= 44]
# dfBelem_FW = dfBelem[dfBelem['SEM_PRI'] <= 44]
# dfPortoVelho_FW = dfPortoVelho[dfPortoVelho['SEM_PRI'] <= 44]
# dfSalvador_FW = dfSalvador[dfSalvador['SEM_PRI'] <= 44]
# dfRecife_FW = dfRecife[dfRecife['SEM_PRI'] <= 44]
dfFortaleza_FW = dfFortaleza[dfFortaleza['SEM_PRI'] <= ondaInicio]

# dfCuritiba_SW = dfCuritiba[dfCuritiba['SEM_PRI'] > 44]
# dfPortoAlegre_SW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] > 44]
# dfSaoPaulo_SW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] > 44]
# dfBeloHorizonte_SW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] > 44]
# dfManaus_SW = dfManaus[dfManaus['SEM_PRI'] > 44]
# dfBelem_SW = dfBelem[dfBelem['SEM_PRI'] > 44]
# dfPortoVelho_SW = dfPortoVelho[dfPortoVelho['SEM_PRI'] > 44]
# dfSalvador_SW = dfSalvador[dfSalvador['SEM_PRI'] > 44]
# dfRecife_SW = dfRecife[dfRecife['SEM_PRI'] > 44]
dfFortaleza_SW = dfFortaleza[dfFortaleza['SEM_PRI'] > ondaInicio]
# --------------------------------------------------------------------------------------#
#############################################################################################################################

dfTotalCasos_FW = dfFortaleza_FW[['SEM_PRI', 'DT_NOTIFIC']].groupby(by='SEM_PRI').count()
dfTotalCasos_FW.reset_index(inplace=True)
dfTotalCasos_FW.columns =['SEM_PRI', 'CASOS']

dfTotalCasos_SW = dfFortaleza_SW[['SEM_PRI', 'DT_NOTIFIC']].groupby(by='SEM_PRI').count()
dfTotalCasos_SW.reset_index(inplace=True)
dfTotalCasos_SW.columns =['SEM_PRI', 'CASOS']
#############################################################################################################################


dfCidade_FW_jovens = dfFortaleza_FW[(dfFortaleza_FW['NU_IDADE_N'] < 40)]
dfCidade_FW_meia = dfFortaleza_FW[(dfFortaleza_FW['NU_IDADE_N'] < 60) & (dfFortaleza_FW['NU_IDADE_N'] >= 40)]
dfCidade_FW_idosos = dfFortaleza_FW[dfFortaleza_FW['NU_IDADE_N'] >= 60]

dfCidade_SW_jovens = dfFortaleza_SW[(dfFortaleza_SW['NU_IDADE_N'] < 40)]
dfCidade_SW_meia = dfFortaleza_SW[(dfFortaleza_SW['NU_IDADE_N'] < 60) & (dfFortaleza_SW['NU_IDADE_N'] >= 40)]
dfCidade_SW_idosos = dfFortaleza_SW[dfFortaleza_SW['NU_IDADE_N'] >= 60]

dfCount_FW = dfCidade_FW_jovens[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount_FW.reset_index(inplace=True)
dfCount2_FW = dfCidade_FW_idosos[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount2_FW.reset_index(inplace=True)
dfCount3_FW = dfCidade_FW_meia[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount3_FW.reset_index(inplace=True)
#############################################################################################################################

aux = pd.DataFrame()
piroca = dfCount_FW['SEM_PRI'].tolist()

for i in dfTotalCasos_FW.SEM_PRI:
    if not(i in piroca):
        aux = aux.append([[i, 0]])
if not(aux.empty):
    aux.columns = ['SEM_PRI', 'NU_IDADE_N']
    dfCount_FW = dfCount_FW.append(aux)
    dfCount_FW.sort_values(by='SEM_PRI', inplace=True)
    dfCount_FW.reset_index(inplace = True)

aux2 = pd.DataFrame()
piroca2 = dfCount2_FW['SEM_PRI'].tolist()

for i in dfTotalCasos_FW.SEM_PRI:
    if not(i in piroca2):
        aux2 = aux2.append([[i, 0]])
if not(aux2.empty):
    aux2.columns = ['SEM_PRI', 'NU_IDADE_N']
    dfCount2_FW = dfCount2_FW.append(aux2)
    dfCount2_FW.sort_values(by='SEM_PRI', inplace=True)
    dfCount2_FW.reset_index(inplace = True)

aux3 = pd.DataFrame()
piroca3 = dfCount3_FW['SEM_PRI'].tolist()

for i in dfTotalCasos_FW.SEM_PRI:
    if not(i in piroca3):
        aux3 = aux3.append([[i, 0]])
if not(aux3.empty):
    aux3.columns = ['SEM_PRI', 'NU_IDADE_N']
    dfCount3_FW = dfCount3_FW.append(aux3)
    dfCount3_FW.sort_values(by='SEM_PRI', inplace=True)
    dfCount3_FW.reset_index(inplace = True)

#############################################################################################################################

dfCount_FW['IDADE_NORM'] = (dfCount_FW['NU_IDADE_N'] / dfTotalCasos_FW['CASOS'])*100
dfCount2_FW['IDADE_NORM'] = (dfCount2_FW['NU_IDADE_N'] / dfTotalCasos_FW['CASOS'])*100
dfCount3_FW['IDADE_NORM'] = (dfCount3_FW['NU_IDADE_N'] / dfTotalCasos_FW['CASOS'])*100

dfCount_SW = dfCidade_SW_jovens[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount_SW.reset_index(inplace=True)
dfCount2_SW = dfCidade_SW_idosos[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount2_SW.reset_index(inplace=True)
dfCount3_SW = dfCidade_SW_meia[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount3_SW.reset_index(inplace=True)

dfCount_SW['IDADE_NORM'] = (dfCount_SW['NU_IDADE_N'] / dfTotalCasos_SW['CASOS'])*100
dfCount2_SW['IDADE_NORM'] = (dfCount2_SW['NU_IDADE_N'] / dfTotalCasos_SW['CASOS'])*100
dfCount3_SW['IDADE_NORM'] = (dfCount3_SW['NU_IDADE_N'] / dfTotalCasos_SW['CASOS'])*100
# --------------------------------------------------------------------------------------#

x_FW = dfCount_FW['SEM_PRI']
x2_FW = dfCount2_FW['SEM_PRI']
x3_FW = dfCount3_FW['SEM_PRI']

y_FW = dfCount_FW['IDADE_NORM']
y2_FW = dfCount2_FW['IDADE_NORM']
y3_FW = dfCount3_FW['IDADE_NORM']

x_SW = dfCount_SW['SEM_PRI']
x2_SW = dfCount2_SW['SEM_PRI']
x3_SW = dfCount3_SW['SEM_PRI']

y_SW = dfCount_SW['IDADE_NORM']
y2_SW = dfCount2_SW['IDADE_NORM']
y3_SW = dfCount3_SW['IDADE_NORM']

X_FW = x_FW[:, np.newaxis]
X2_FW = x2_FW[:, np.newaxis]
X3_FW = x3_FW[:, np.newaxis]

X_SW = x_SW[:, np.newaxis]
X2_SW = x2_SW[:, np.newaxis]
X3_SW = x3_SW[:, np.newaxis]
# --------------------------------------------------------------------------------------#

# -> REGRESSION 1 WAVE
X_FW_train, X_FW_test, y_FW_train, y_FW_test = train_test_split(X_FW, y_FW, test_size=0.4, random_state=42)
X2_FW_train, X2_FW_test, y2_FW_train, y2_FW_test = train_test_split(X2_FW, y2_FW, test_size=0.4, random_state=42)
X3_FW_train, X3_FW_test, y3_FW_train, y3_FW_test = train_test_split(X3_FW, y3_FW, test_size=0.4, random_state=42)
regr_FW = linear_model.LinearRegression()
regr2_FW = linear_model.LinearRegression()
regr3_FW = linear_model.LinearRegression()
regr_FW.fit(X_FW_train, y_FW_train)
regr2_FW.fit(X2_FW_train, y2_FW_train)
regr3_FW.fit(X3_FW_train, y3_FW_train)
y_FW_pred = regr_FW.predict(X_FW_test)
y2_FW_pred = regr2_FW.predict(X2_FW_test)
y3_FW_pred = regr3_FW.predict(X3_FW_test)

# -> REGRESSION 2 WAVE
X_SW_train, X_SW_test, y_SW_train, y_SW_test = train_test_split(X_SW, y_SW, test_size=0.4, random_state=42)
X2_SW_train, X2_SW_test, y2_SW_train, y2_SW_test = train_test_split(X2_SW, y2_SW, test_size=0.4, random_state=42)
X3_SW_train, X3_SW_test, y3_SW_train, y3_SW_test = train_test_split(X3_SW, y3_SW, test_size=0.4, random_state=42)
regr_SW = linear_model.LinearRegression()
regr2_SW = linear_model.LinearRegression()
regr3_SW = linear_model.LinearRegression()
regr_SW.fit(X_SW_train, y_SW_train)
regr2_SW.fit(X2_SW_train, y2_SW_train)
regr3_SW.fit(X3_SW_train, y3_SW_train)
y_SW_pred = regr_SW.predict(X_SW_test)
y2_SW_pred = regr2_SW.predict(X2_SW_test)
y3_SW_pred = regr3_SW.predict(X3_SW_test)

print('*********************************************************************')
print('PRIMEIRA ONDA')
print()
X_FW_P = sm.add_constant(X_FW)
est_FW = sm.OLS(y_FW, X_FW_P)
est_FW_2 = est_FW.fit()
print(est_FW_2.summary())
print()
print('---------------------------------------------------------------------')
X2_FW_P = sm.add_constant(X2_FW)
est2_FW = sm.OLS(y2_FW, X2_FW_P)
est2_FW_2 = est2_FW.fit()
print(est2_FW_2.summary())
print()
print('---------------------------------------------------------------------')
X3_FW_P = sm.add_constant(X3_FW)
est3_FW = sm.OLS(y3_FW, X3_FW_P)
est3_FW_2 = est3_FW.fit()
print(est3_FW_2.summary())
print()
print('_____________________________________________________________________')
print('SEGUNDA ONDA')
print()
X_SW_P = sm.add_constant(X_SW)
est_SW = sm.OLS(y_SW, X_SW_P)
est_SW_2 = est_SW.fit()
print(est_SW_2.summary())
print()
print('---------------------------------------------------------------------')
X2_SW_P = sm.add_constant(X2_SW)
est2_SW = sm.OLS(y2_SW, X2_SW_P)
est2_SW_2 = est2_SW.fit()
print(est2_SW_2.summary())
print()
print('---------------------------------------------------------------------')
X3_SW_P = sm.add_constant(X3_SW)
est3_SW = sm.OLS(y3_SW, X3_SW_P)
est3_SW_2 = est3_SW.fit()
print(est3_SW_2.summary())
print()
print('---------------------------------------------------------------------')
# --------------------------------------------------------------------------------------#

# The coefficients
print('*********************************************************************')
print()
print('---------------------------------------------------------------------')
print('Coefficients_FW: \n', regr_FW.coef_)
print('Coefficients1_FW: \n', regr2_FW.coef_)
print('Coefficients2_FW: \n', regr3_FW.coef_)
print()
print('Coefficients_SW: \n', regr_SW.coef_)
print('Coefficients2_SW: \n', regr2_SW.coef_)
print('Coefficients3_SW: \n', regr3_SW.coef_)
print('---------------------------------------------------------------------')
# The mean squared error
print()
print('---------------------------------------------------------------------')
print('Mean squared error_FW: %.2f' % mean_squared_error(y_FW_test, y_FW_pred))
print('Mean squared error2_FW: %.2f' % mean_squared_error(y2_FW_test, y2_FW_pred))
print('Mean squared error3_FW: %.2f' % mean_squared_error(y3_FW_test, y3_FW_pred))
print()
print('Mean squared error_SW: %.2f' % mean_squared_error(y_SW_test, y_SW_pred))
print('Mean squared error2_SW: %.2f' % mean_squared_error(y2_SW_test, y2_SW_pred))
print('Mean squared error3_: %.2f' % mean_squared_error(y3_SW_test, y3_SW_pred))
print('---------------------------------------------------------------------')
# The coefficient of determination: 1 is perfect prediction
print()
print('---------------------------------------------------------------------')
print('Coefficient of determination_FW: %.2f' % r2_score(y_FW_test, y_FW_pred))
print('Coefficient of determination2_FW: %.2f' % r2_score(y2_FW_test, y2_FW_pred))
print('Coefficient of determination3_FW: %.2f' % r2_score(y3_FW_test, y3_FW_pred))
print()
print('Coefficient of determination_SW: %.2f' % r2_score(y_SW_test, y_SW_pred))
print('Coefficient of determination2_SW: %.2f' % r2_score(y2_SW_test, y2_SW_pred))
print('Coefficient of determination3_SW: %.2f' % r2_score(y3_SW_test, y3_SW_pred))
print('---------------------------------------------------------------------')
# --------------------------------------------------------------------------------------#

fig, ax = plt.subplots(figsize=(20, 10))
plt.title("Composição dos casos graves de COVID19 - Primeira onda em Fortaleza")
ax.set_ylabel('Quantidade de casos (%)')
ax.set_xlabel('Semana epidemiologica')
ax.set_ylim([-5, 105])
ax.scatter(X_FW, y_FW, color='red', label='Jovens')
ax.scatter(X3_FW, y3_FW, color='green', label='Meia Idade')
ax.scatter(X2_FW, y2_FW, color='blue', label='Idosos')
plt.plot(X_FW_test, y_FW_pred, color='indianred')
plt.plot(X2_FW_test, y2_FW_pred, color='cornflowerblue')
plt.plot(X3_FW_test, y3_FW_pred, color='mediumspringgreen')
ax.legend()
plt.savefig('ImagensFinal/Relatorio/primeiraOndaFT.png')
# plt.show()
fig, ax = plt.subplots(figsize=(20, 10))
plt.title("Composição dos casos graves de COVID19 - Segunda onda em Fortaleza")
ax.set_ylabel('Quantidade de casos (%)')
ax.set_xlabel('Semana epidemiologica')
ax.set_ylim([-5, 105])
# ax.set_xticks(range(X_SW.min(), X_SW.max()+1, 4))
ax.scatter(X_SW, y_SW, color='red', label='Jovens')
ax.scatter(X3_SW, y3_SW, color='green', label='Meia Idade')
ax.scatter(X2_SW, y2_SW, color='blue', label='Idosos')
plt.plot(X_SW_test, y_SW_pred, color='indianred')
plt.plot(X2_SW_test, y2_SW_pred, color='cornflowerblue')
plt.plot(X3_SW_test, y3_SW_pred, color='mediumspringgreen')
ax.legend()
plt.savefig('ImagensFinal/Relatorio/segundaOndaFT.png')
plt.show()
