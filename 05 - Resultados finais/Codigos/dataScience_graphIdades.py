import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import datetime

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from matplotlib.ticker import MaxNLocator
from sklearn.pipeline import make_pipeline
from matplotlib import dates

pd.options.display.max_columns = None
pd.options.display.max_rows = None
data2021 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data2020 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data20_21 = data2020
data20_21.append(data2021, ignore_index=True)

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '05/08/4020']
dfGeral20_21.drop(filtro.index, inplace=True)
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '19/11/2929']
dfGeral20_21.drop(filtro.index, inplace=True)

dfGeral20_21['DT_NOTIFIC'] = pd.to_datetime(dfGeral20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfGeral20_21['DT_SIN_PRI'] = pd.to_datetime(dfGeral20_21['DT_SIN_PRI'], format='%d/%m/%Y')
dfGeral20_21['DT_NASC'] = pd.to_datetime(dfGeral20_21['DT_NASC'], format='%d/%m/%Y')
dfGeral20_21['DT_ENTUTI'] = pd.to_datetime(dfGeral20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfGeral20_21['DT_SAIDUTI'] = pd.to_datetime(dfGeral20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfGeral20_21[dfGeral20_21.DT_NOTIFIC.dt.year > 2021]
dfGeral20_21.drop(filtro.index, inplace=True)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2021) & (dfGeral20_21.DT_NOTIFIC.dt.month > 4)]
dfGeral20_21.drop(filtro.index, inplace=True)

print (dfGeral20_21.head())

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2020) & (dfGeral20_21.DT_NOTIFIC.dt.month < 4)]
dfGeral20_21.drop(filtro.index, inplace=True)

dfCuritiba = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'CURITIBA']
dfPortoAlegre = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO ALEGRE']
dfSaoPaulo = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SAO PAULO']
dfBeloHorizonte = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELO HORIZONTE']
dfManaus = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'MANAUS']
dfBelem = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELEM']
dfPortoVelho = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO VELHO']
dfSalvador = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SALVADOR']
dfRecife = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'RECIFE']
dfFortaleza = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'FORTALEZA']

dfCuritiba_FW = dfCuritiba[dfCuritiba['SEM_PRI'] <= 44]
dfPortoAlegre_FW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] <= 44]
dfSaoPaulo_FW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] <= 44]
dfBeloHorizonte_FW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] <= 44]
dfManaus_FW = dfManaus[dfManaus['SEM_PRI'] <= 44]
dfBelem_FW = dfBelem[dfBelem['SEM_PRI'] <= 44]
dfPortoVelho_FW = dfPortoVelho[dfPortoVelho['SEM_PRI'] <= 44]
dfSalvador_FW = dfSalvador[dfSalvador['SEM_PRI'] <= 44]
dfRecife_FW = dfRecife[dfRecife['SEM_PRI'] <= 44]
dfFortaleza_FW = dfFortaleza[dfFortaleza['SEM_PRI'] <= 44]

dfCuritiba_SW = dfCuritiba[dfCuritiba['SEM_PRI'] > 44]
dfPortoAlegre_SW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] > 44]
dfSaoPaulo_SW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] > 44]
dfBeloHorizonte_SW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] > 44]
dfManaus_SW = dfManaus[dfManaus['SEM_PRI'] > 44]
dfBelem_SW = dfBelem[dfBelem['SEM_PRI'] > 44]
dfPortoVelho_SW = dfPortoVelho[dfPortoVelho['SEM_PRI'] > 44]
dfSalvador_SW = dfSalvador[dfSalvador['SEM_PRI'] > 44]
dfRecife_SW = dfRecife[dfRecife['SEM_PRI'] > 44]
dfFortaleza_SW = dfFortaleza[dfFortaleza['SEM_PRI'] > 44]

IdadeMediaCuritiba_FW = dfCuritiba_FW['NU_IDADE_N'].mean()
IdadeMediaPortoAlegre_FW = dfPortoAlegre_FW['NU_IDADE_N'].mean()
IdadeMediaSaoPaulo_FW = dfSaoPaulo_FW['NU_IDADE_N'].mean()
IdadeMediaBeloHorizonte_FW = dfBeloHorizonte_FW['NU_IDADE_N'].mean()
IdadeMediaManaus_FW = dfManaus_FW['NU_IDADE_N'].mean()
IdadeMediaBelem_FW = dfBelem_FW['NU_IDADE_N'].mean()
IdadeMediaPortoVelho_FW = dfPortoVelho_FW['NU_IDADE_N'].mean()
IdadeMediaSalvador_FW = dfSalvador_FW['NU_IDADE_N'].mean()
IdadeMediaRecife_FW = dfRecife_FW['NU_IDADE_N'].mean()
IdadeMediaFortaleza_FW = dfFortaleza_FW['NU_IDADE_N'].mean()

IdadeMediaCuritiba_SW = dfCuritiba_SW['NU_IDADE_N'].mean()
IdadeMediaPortoAlegre_SW = dfPortoAlegre_SW['NU_IDADE_N'].mean()
IdadeMediaSaoPaulo_SW = dfSaoPaulo_SW['NU_IDADE_N'].mean()
IdadeMediaBeloHorizonte_SW = dfBeloHorizonte_SW['NU_IDADE_N'].mean()
IdadeMediaManaus_SW = dfManaus_SW['NU_IDADE_N'].mean()
IdadeMediaBelem_SW = dfBelem_SW['NU_IDADE_N'].mean()
IdadeMediaPortoVelho_SW = dfPortoVelho_SW['NU_IDADE_N'].mean()
IdadeMediaSalvador_SW = dfSalvador_SW['NU_IDADE_N'].mean()
IdadeMediaRecife_SW = dfRecife_SW['NU_IDADE_N'].mean()
IdadeMediaFortaleza_SW = dfFortaleza_SW['NU_IDADE_N'].mean()

labels = ['Curitiba', 'Porto Alegre', 'Sao Paulo', 'Belo Horizonte', 'Manaus', 'Belem*', 'Porto Velho', 'Salvador', 'Recife*', 'Fortaleza*']
width = 0.35

fig, ax = plt.subplots(figsize=(20, 10))

ax.bar(1 - width/2, IdadeMediaCuritiba_FW, width, color='cornFlowerblue', label='Primeira onda')
ax.bar(1 + width/2, IdadeMediaCuritiba_SW, width, color='teal', label='Segunda onda')

ax.bar(2 - width/2, IdadeMediaPortoAlegre_FW, width, color='cornFlowerblue')
ax.bar(2 + width/2, IdadeMediaPortoAlegre_SW, width, color='teal')

ax.bar(3 - width/2, IdadeMediaSaoPaulo_FW, width, color='cornFlowerblue')
ax.bar(3 + width/2, IdadeMediaSaoPaulo_SW, width, color='teal')

ax.bar(4 - width/2, IdadeMediaBeloHorizonte_FW, width, color='cornFlowerblue')
ax.bar(4 + width/2, IdadeMediaBeloHorizonte_SW, width, color='teal')

ax.bar(5 - width/2, IdadeMediaManaus_FW, width, color='cornFlowerblue')
ax.bar(5 + width/2, IdadeMediaManaus_SW, width, color='teal')

ax.bar(6 - width/2, IdadeMediaBelem_FW, width, color='cornFlowerblue')
ax.bar(6 + width/2, IdadeMediaBelem_SW, width, color='teal')

ax.bar(7 - width/2, IdadeMediaPortoVelho_FW, width, color='cornFlowerblue')
ax.bar(7 + width/2, IdadeMediaPortoVelho_SW, width, color='teal')

ax.bar(8 - width/2, IdadeMediaSalvador_FW, width, color='cornFlowerblue')
ax.bar(8 + width/2, IdadeMediaSalvador_SW, width, color='teal')

ax.bar(9 - width/2, IdadeMediaSalvador_FW, width, color='cornFlowerblue')
ax.bar(9 + width/2, IdadeMediaSalvador_SW, width, color='teal')

ax.bar(10 - width/2, IdadeMediaFortaleza_FW, width, color='cornFlowerblue')
ax.bar(10 + width/2, IdadeMediaFortaleza_SW, width, color='teal')


# plt.locator_params(axis="x", nbins=4)

# plt.legend(loc='upper left')
plt.title("Media de idade")
# ax.xaxis.set_major_locator(MaxNLocator(20))
ax.tick_params(axis='x', rotation=45)
ax.set_xticklabels(labels)
ax.set_xticks(np.arange(len(labels)) + 1)
ax.legend()

plt.show()