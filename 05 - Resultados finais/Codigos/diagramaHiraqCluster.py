import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import datetime

from matplotlib import dates

pd.options.display.max_columns = None
pd.options.display.max_rows = None

data2021 = pd.read_csv('dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data2020 = pd.read_csv('dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])

data20_21 = data2020
data20_21.append(data2021, ignore_index=True)

dfUTI20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3) & (data20_21['UTI'] == 1)]
dfUTI20_21.dropna(subset=['DT_ENTUTI'], inplace=True)

filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '05/08/4020']
dfUTI20_21.drop(filtro.index, inplace=True)

filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '19/11/2929']
dfUTI20_21.drop(filtro.index, inplace=True)

dfUTI20_21['DT_NOTIFIC'] = pd.to_datetime(dfUTI20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfUTI20_21['DT_NASC'] = pd.to_datetime(dfUTI20_21['DT_NASC'], format='%d/%m/%Y')
dfUTI20_21['DT_ENTUTI'] = pd.to_datetime(dfUTI20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfUTI20_21['DT_SAIDUTI'] = pd.to_datetime(dfUTI20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfUTI20_21[dfUTI20_21.DT_ENTUTI.dt.year > 2021]
dfUTI20_21.drop(filtro.index, inplace=True)

filtro = dfUTI20_21[(dfUTI20_21.DT_ENTUTI.dt.year == 2021) & (dfUTI20_21.DT_ENTUTI.dt.month > 4)]
dfUTI20_21.drop(filtro.index, inplace=True)


filtro = dfUTI20_21[dfUTI20_21.DT_NOTIFIC.dt.year > 2021]
dfUTI20_21.drop(filtro.index, inplace=True)

dfUTI20_21.sort_values(by=['DT_NOTIFIC'])

dfUTI20_21.reset_index(inplace=True)

# dfGroup = dfUTI20_21[['NU_IDADE_N' , 'DT_ENTUTI']].groupby(by='DT_ENTUTI').mean()
# dfGroup.reset_index(inplace=True)

# # print(dfGroup.sort_values(by=['DT_ENTUTI']))

# fig = plt.subplots(figsize=(20, 10))
# plt.scatter(dfGroup['DT_ENTUTI'], dfGroup['NU_IDADE_N'])

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
dfCount = dfGeral20_21[['DT_NOTIFIC' , 'CS_SEXO']].groupby(by='DT_NOTIFIC').count()

import numpy as np

from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import load_iris
from sklearn.cluster import AgglomerativeClustering

def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_, counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)

X = dfUTI20_21[['DT_ENTUTI' , 'NU_IDADE_N']][:55000]
X['DT_ENTUTI'] = dates.date2num(X['DT_ENTUTI'])
X = X.to_numpy()

# setting distance_threshold=0 ensures we compute the full tree.
model = AgglomerativeClustering(distance_threshold=0, n_clusters=None)

model = model.fit(X)
plt.title('Hierarchical Clustering Dendrogram')
# plot the top three levels of the dendrogram
plot_dendrogram(model, truncate_mode='level', p=3)
plt.xlabel("Number of points in node (or index of point if no parenthesis).")
plt.show()