import matplotlib.pyplot    as plt
import pandas               as pd
import numpy                as np
import datetime

from matplotlib                 import dates
from sklearn.model_selection    import train_test_split
from sklearn.metrics            import mean_squared_error, r2_score
from sklearn                    import linear_model

# --------------------------------------------------------------------------------------#
pd.options.display.max_columns = None
pd.options.display.max_rows = None
data2021 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'], low_memory=False)
data2020 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'], low_memory=False)
data20_21 = data2020
data20_21.append(data2021, ignore_index=True)
# --------------------------------------------------------------------------------------#

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '05/08/4020']
dfGeral20_21 = dfGeral20_21.drop(filtro.index)
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '19/11/2929']
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

dfGeral20_21['DT_NOTIFIC'] = pd.to_datetime(dfGeral20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfGeral20_21['DT_SIN_PRI'] = pd.to_datetime(dfGeral20_21['DT_SIN_PRI'], format='%d/%m/%Y')
dfGeral20_21['DT_NASC'] = pd.to_datetime(dfGeral20_21['DT_NASC'], format='%d/%m/%Y')
dfGeral20_21['DT_ENTUTI'] = pd.to_datetime(dfGeral20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfGeral20_21['DT_SAIDUTI'] = pd.to_datetime(dfGeral20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfGeral20_21[dfGeral20_21.DT_NOTIFIC.dt.year > 2021]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2021) & (dfGeral20_21.DT_NOTIFIC.dt.month > 4)]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2020) & (dfGeral20_21.DT_NOTIFIC.dt.month < 4)]
dfGeral20_21 = dfGeral20_21.drop(filtro.index)
# --------------------------------------------------------------------------------------#

# dfCuritiba = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'CURITIBA']
# dfPortoAlegre = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO ALEGRE']
# dfSaoPaulo = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SAO PAULO']
dfBeloHorizonte = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELO HORIZONTE']
# dfManaus = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'MANAUS']
# dfBelem = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'BELEM']
# dfPortoVelho = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO VELHO']
# dfSalvador = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'SALVADOR']
# dfRecife = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'RECIFE']
# dfFortaleza = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'FORTALEZA']

# dfCuritiba_FW = dfCuritiba[dfCuritiba['SEM_PRI'] <= 44]
# dfPortoAlegre_FW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] <= 44]
# dfSaoPaulo_FW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] <= 44]
dfBeloHorizonte_FW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] <= 44]
# dfManaus_FW = dfManaus[dfManaus['SEM_PRI'] <= 44]
# dfBelem_FW = dfBelem[dfBelem['SEM_PRI'] <= 44]
# dfPortoVelho_FW = dfPortoVelho[dfPortoVelho['SEM_PRI'] <= 44]
# dfSalvador_FW = dfSalvador[dfSalvador['SEM_PRI'] <= 44]
# dfRecife_FW = dfRecife[dfRecife['SEM_PRI'] <= 44]
# dfFortaleza_FW = dfFortaleza[dfFortaleza['SEM_PRI'] <= 44]

# dfCuritiba_SW = dfCuritiba[dfCuritiba['SEM_PRI'] > 44]
# dfPortoAlegre_SW = dfPortoAlegre[dfPortoAlegre['SEM_PRI'] > 44]
# dfSaoPaulo_SW = dfSaoPaulo[dfSaoPaulo['SEM_PRI'] > 44]
dfBeloHorizonte_SW = dfBeloHorizonte[dfBeloHorizonte['SEM_PRI'] > 44]
# dfManaus_SW = dfManaus[dfManaus['SEM_PRI'] > 44]
# dfBelem_SW = dfBelem[dfBelem['SEM_PRI'] > 44]
# dfPortoVelho_SW = dfPortoVelho[dfPortoVelho['SEM_PRI'] > 44]
# dfSalvador_SW = dfSalvador[dfSalvador['SEM_PRI'] > 44]
# dfRecife_SW = dfRecife[dfRecife['SEM_PRI'] > 44]
# dfFortaleza_SW = dfFortaleza[dfFortaleza['SEM_PRI'] > 44]
# --------------------------------------------------------------------------------------#

dfCidade_FW_jovens = dfBeloHorizonte_FW[(dfBeloHorizonte_FW['NU_IDADE_N'] < 40) & (dfBeloHorizonte_FW['NU_IDADE_N'] >= 20)]
dfCidade_FW_meia = dfBeloHorizonte_FW[(dfBeloHorizonte_FW['NU_IDADE_N'] < 60) & (dfBeloHorizonte_FW['NU_IDADE_N'] >= 40)]
dfCidade_FW_idosos = dfBeloHorizonte_FW[dfBeloHorizonte_FW['NU_IDADE_N'] >= 60]

dfCidade_SW_jovens = dfBeloHorizonte_SW[(dfBeloHorizonte_SW['NU_IDADE_N'] < 40) & (dfBeloHorizonte_SW['NU_IDADE_N'] >= 20)]
dfCidade_SW_meia = dfBeloHorizonte_SW[(dfBeloHorizonte_SW['NU_IDADE_N'] < 60) & (dfBeloHorizonte_SW['NU_IDADE_N'] >= 40)]
dfCidade_SW_idosos = dfBeloHorizonte_SW[dfBeloHorizonte_SW['NU_IDADE_N'] >= 60]

dfCount_FW = dfCidade_FW_jovens[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount_FW.reset_index(inplace = True)
dfCount2_FW = dfCidade_FW_idosos[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount2_FW.reset_index(inplace = True)
dfCount3_FW = dfCidade_FW_meia[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount3_FW.reset_index(inplace = True)

# dfCount_FW['IDADE_NORM'] = (dfCount_FW['NU_IDADE_N'] - dfCount_FW['NU_IDADE_N'].min())/(dfCount_FW['NU_IDADE_N'].max() - dfCount_FW['NU_IDADE_N'].min())
# dfCount2_FW['IDADE_NORM'] = (dfCount2_FW['NU_IDADE_N'] - dfCount2_FW['NU_IDADE_N'].min())/(dfCount2_FW['NU_IDADE_N'].max() - dfCount2_FW['NU_IDADE_N'].min())
# dfCount3_FW['IDADE_NORM'] = (dfCount3_FW['NU_IDADE_N'] - dfCount3_FW['NU_IDADE_N'].min())/(dfCount3_FW['NU_IDADE_N'].max() - dfCount3_FW['NU_IDADE_N'].min())

dfCount_SW = dfCidade_SW_jovens[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount_SW.reset_index(inplace = True)
dfCount2_SW = dfCidade_SW_idosos[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount2_SW.reset_index(inplace = True)
dfCount3_SW = dfCidade_SW_meia[['SEM_PRI', 'NU_IDADE_N']].groupby(by='SEM_PRI').count()
dfCount3_SW.reset_index(inplace = True)

# dfCount_SW['IDADE_NORM'] = (dfCount_SW['NU_IDADE_N'] - dfCount_SW['NU_IDADE_N'].min())/(dfCount_SW['NU_IDADE_N'].max() - dfCount_SW['NU_IDADE_N'].min())
# dfCount2_SW['IDADE_NORM'] = (dfCount2_SW['NU_IDADE_N'] - dfCount2_SW['NU_IDADE_N'].min())/(dfCount2_SW['NU_IDADE_N'].max() - dfCount2_SW['NU_IDADE_N'].min())
# dfCount3_SW['IDADE_NORM'] = (dfCount3_SW['NU_IDADE_N'] - dfCount3_SW['NU_IDADE_N'].min())/(dfCount3_SW['NU_IDADE_N'].max() - dfCount3_SW['NU_IDADE_N'].min())
# --------------------------------------------------------------------------------------#
x_FW = dfCount_FW['SEM_PRI']
x2_FW = dfCount2_FW['SEM_PRI']
x3_FW = dfCount3_FW['SEM_PRI']

y_FW = dfCount_FW['NU_IDADE_N']
y2_FW = dfCount2_FW['NU_IDADE_N']
y3_FW = dfCount3_FW['NU_IDADE_N']

x_SW = dfCount_SW['SEM_PRI']
x2_SW = dfCount2_SW['SEM_PRI']
x3_SW = dfCount3_SW['SEM_PRI']

y_SW = dfCount_SW['NU_IDADE_N']
y2_SW = dfCount2_SW['NU_IDADE_N']
y3_SW = dfCount3_SW['NU_IDADE_N']

X_FW = x_FW[:, np.newaxis]
X2_FW = x2_FW[:, np.newaxis]
X3_FW = x3_FW[:, np.newaxis]

X_SW = x_SW[:, np.newaxis]
X2_SW = x2_SW[:, np.newaxis]
X3_SW = x3_SW[:, np.newaxis]
# --------------------------------------------------------------------------------------#

# -> REGRESSION 1 WAVE
X_FW_train, X_FW_test, y_FW_train, y_FW_test = train_test_split(X_FW, y_FW, test_size=0.4, random_state=42)
X2_FW_train, X2_FW_test, y2_FW_train, y2_FW_test = train_test_split(X2_FW, y2_FW, test_size=0.4, random_state=42)
X3_FW_train, X3_FW_test, y3_FW_train, y3_FW_test = train_test_split(X3_FW, y3_FW, test_size=0.4, random_state=42)
regr_FW = linear_model.LinearRegression()
regr2_FW = linear_model.LinearRegression()
regr3_FW = linear_model.LinearRegression()
regr_FW.fit(X_FW_train, y_FW_train)
regr2_FW.fit(X2_FW_train, y2_FW_train)
regr3_FW.fit(X3_FW_train, y3_FW_train)
y_FW_pred = regr_FW.predict(X_FW_test)
y2_FW_pred = regr2_FW.predict(X2_FW_test)
y3_FW_pred = regr3_FW.predict(X3_FW_test)

# -> REGRESSION 2 WAVE
X_SW_train, X_SW_test, y_SW_train, y_SW_test = train_test_split(X_SW, y_SW, test_size=0.4, random_state=42)
X2_SW_train, X2_SW_test, y2_SW_train, y2_SW_test = train_test_split(X2_SW, y2_SW, test_size=0.4, random_state=42)
X3_SW_train, X3_SW_test, y3_SW_train, y3_SW_test = train_test_split(X3_SW, y3_SW, test_size=0.4, random_state=42)
regr_SW = linear_model.LinearRegression()
regr2_SW = linear_model.LinearRegression()
regr3_SW = linear_model.LinearRegression()
regr_SW.fit(X_SW_train, y_SW_train)
regr2_SW.fit(X2_SW_train, y2_SW_train)
regr3_SW.fit(X3_SW_train, y3_SW_train)
y_SW_pred = regr_SW.predict(X_SW_test)
y2_SW_pred = regr2_SW.predict(X2_SW_test)
y3_SW_pred = regr3_SW.predict(X3_SW_test)
# --------------------------------------------------------------------------------------#

# The coefficients
print('*********************************************************************')
print()
print('---------------------------------------------------------------------')
print('Coefficients_FW: \n', regr_FW.coef_)
print('Coefficients1_FW: \n', regr2_FW.coef_)
print('Coefficients2_FW: \n', regr3_FW.coef_)
print()
print('Coefficients_SW: \n', regr_SW.coef_)
print('Coefficients2_SW: \n', regr2_SW.coef_)
print('Coefficients3_SW: \n', regr3_SW.coef_)
print('---------------------------------------------------------------------')
# The mean squared error
print()
print('---------------------------------------------------------------------')
print('Mean squared error_FW: %.2f' % mean_squared_error(y_FW_test, y_FW_pred))
print('Mean squared error2_FW: %.2f' % mean_squared_error(y2_FW_test, y2_FW_pred))
print('Mean squared error3_FW: %.2f' % mean_squared_error(y3_FW_test, y3_FW_pred))
print()
print('Mean squared error_SW: %.2f' % mean_squared_error(y_SW_test, y_SW_pred))
print('Mean squared error2_SW: %.2f' % mean_squared_error(y2_SW_test, y2_SW_pred))
print('Mean squared error3_: %.2f' % mean_squared_error(y3_SW_test, y3_SW_pred))
print('---------------------------------------------------------------------')
# The coefficient of determination: 1 is perfect prediction
print()
print('---------------------------------------------------------------------')
print('Coefficient of determination_FW: %.2f' % r2_score(y_FW_test, y_FW_pred))
print('Coefficient of determination2_FW: %.2f' % r2_score(y2_FW_test, y2_FW_pred))
print('Coefficient of determination3_FW: %.2f' % r2_score(y3_FW_test, y3_FW_pred))
print()
print('Coefficient of determination_SW: %.2f' % r2_score(y_SW_test, y_SW_pred))
print('Coefficient of determination2_SW: %.2f' % r2_score(y2_SW_test, y2_SW_pred))
print('Coefficient of determination3_SW: %.2f' % r2_score(y3_SW_test, y3_SW_pred))
print('---------------------------------------------------------------------')
# --------------------------------------------------------------------------------------#

fig, ax = plt.subplots(figsize=(20, 10))
plt.title("Quantidade de casos primeira onda Belo Horizonte")
ax.set_ylabel('Quantidade de casos')
ax.set_xlabel('Semana epidemiologica')
ax.scatter(X_FW, y_FW, color='red', label='Jovens')
ax.scatter(X2_FW, y2_FW, color='blue', label='Idosos')
ax.scatter(X3_FW, y3_FW, color='green', label='Meia Idade')
plt.plot(X_FW_test, y_FW_pred, color='indianred')
plt.plot(X2_FW_test, y2_FW_pred, color='cornflowerblue')
plt.plot(X3_FW_test, y3_FW_pred, color='mediumspringgreen')
ax.legend()
# plt.show()
fig, ax = plt.subplots(figsize=(20, 10))
plt.title("Quantidade de casos segunda onda Belo Horizonte")
ax.set_ylabel('Quantidade de casos')
ax.set_xlabel('Semana epidemiologica')
ax.scatter(X_SW, y_SW, color='red', label='Jovens')
ax.scatter(X2_SW, y2_SW, color='blue', label='Idosos')
ax.scatter(X3_SW, y3_SW, color='green', label='Meia Idade')
plt.plot(X_SW_test, y_SW_pred, color='indianred')
plt.plot(X2_SW_test, y2_SW_pred, color='cornflowerblue')
plt.plot(X3_SW_test, y3_SW_pred, color='mediumspringgreen')
ax.legend()
plt.show()
