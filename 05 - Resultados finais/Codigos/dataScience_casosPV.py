import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import datetime

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Ridge
from matplotlib.ticker import MaxNLocator
from sklearn.pipeline import make_pipeline
from matplotlib import dates

pd.options.display.max_columns = None
pd.options.display.max_rows = None
data2021 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2021.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data2020 = pd.read_csv('~/Documentos/DataScience/dataset/gripe2020.csv', sep=';', usecols=['DT_NOTIFIC', 'DT_SIN_PRI', 'SEM_PRI', 'SG_UF_NOT', 'ID_MUNICIP', 'CS_SEXO', 'DT_NASC', 'NU_IDADE_N', 'TP_IDADE', 'AN_SARS2', 'PCR_SARS2', 'UTI', 'DT_ENTUTI', 'DT_SAIDUTI', 'CLASSI_FIN', 'RES_IGG', 'RES_IGM', 'RES_IGA'])
data20_21 = data2020
data20_21.append(data2021, ignore_index=True)

# dfUTI20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3) & (data20_21['UTI'] == 1)]
# dfUTI20_21.dropna(subset=['DT_ENTUTI'], inplace=True)

# filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '05/08/4020']
# dfUTI20_21.drop(filtro.index, inplace=True)
# filtro = dfUTI20_21[dfUTI20_21['DT_ENTUTI'] == '19/11/2929']
# dfUTI20_21.drop(filtro.index, inplace=True)

# dfUTI20_21['DT_NOTIFIC'] = pd.to_datetime(dfUTI20_21['DT_NOTIFIC'], format='%d/%m/%Y')
# dfUTI20_21['DT_NASC'] = pd.to_datetime(dfUTI20_21['DT_NASC'], format='%d/%m/%Y')
# dfUTI20_21['DT_ENTUTI'] = pd.to_datetime(dfUTI20_21['DT_ENTUTI'], format='%d/%m/%Y')
# dfUTI20_21['DT_SAIDUTI'] = pd.to_datetime(dfUTI20_21['DT_SAIDUTI'], format='%d/%m/%Y')

# filtro = dfUTI20_21[dfUTI20_21.DT_ENTUTI.dt.year > 2021]
# dfUTI20_21.drop(filtro.index, inplace=True)
# filtro = dfUTI20_21[(dfUTI20_21.DT_ENTUTI.dt.year == 2021) & (dfUTI20_21.DT_ENTUTI.dt.month > 4)]
# dfUTI20_21.drop(filtro.index, inplace=True)
# filtro = dfUTI20_21[dfUTI20_21.DT_NOTIFIC.dt.year > 2021]
# dfUTI20_21.drop(filtro.index, inplace=True)

# dfUTI20_21.sort_values(by=['DT_NOTIFIC'])
# dfUTI20_21.reset_index(inplace=True)

dfGeral20_21 = data20_21.loc[(data20_21['CLASSI_FIN'] == 5) & (data20_21['TP_IDADE'] == 3)]
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '05/08/4020']
dfGeral20_21.drop(filtro.index, inplace=True)
filtro = dfGeral20_21[dfGeral20_21['DT_ENTUTI'] == '19/11/2929']
dfGeral20_21.drop(filtro.index, inplace=True)

dfGeral20_21['DT_NOTIFIC'] = pd.to_datetime(dfGeral20_21['DT_NOTIFIC'], format='%d/%m/%Y')
dfGeral20_21['DT_SIN_PRI'] = pd.to_datetime(dfGeral20_21['DT_SIN_PRI'], format='%d/%m/%Y')
dfGeral20_21['DT_NASC'] = pd.to_datetime(dfGeral20_21['DT_NASC'], format='%d/%m/%Y')
dfGeral20_21['DT_ENTUTI'] = pd.to_datetime(dfGeral20_21['DT_ENTUTI'], format='%d/%m/%Y')
dfGeral20_21['DT_SAIDUTI'] = pd.to_datetime(dfGeral20_21['DT_SAIDUTI'], format='%d/%m/%Y')

filtro = dfGeral20_21[dfGeral20_21.DT_NOTIFIC.dt.year > 2021]
dfGeral20_21.drop(filtro.index, inplace=True)

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2021) & (dfGeral20_21.DT_NOTIFIC.dt.month > 4)]
dfGeral20_21.drop(filtro.index, inplace=True)

print (dfGeral20_21.head())

filtro = dfGeral20_21[(dfGeral20_21.DT_NOTIFIC.dt.year == 2020) & (dfGeral20_21.DT_NOTIFIC.dt.month < 4)]
dfGeral20_21.drop(filtro.index, inplace=True)

# print(dfGeral20_21.head())

dfCidade = dfGeral20_21[dfGeral20_21['ID_MUNICIP'] == 'PORTO VELHO']

# print(dfCuritiba.head())


dfCount = dfCidade[['SEM_PRI' , 'CS_SEXO']].groupby(by='SEM_PRI').count()
dfCount.reset_index(inplace=True)

# print(dfCount.head(100))

x = dfCount['SEM_PRI']
y = dfCount['CS_SEXO']
X = x[:, np.newaxis]
X_plot = x[:, np.newaxis]


# fig = plt.subplots(figsize=(20, 10))
# plt.scatter(x,y)
# plt.show()

colors = ['teal', 'yellowgreen', 'red']
lw = 2
fig, ax = plt.subplots(figsize=(20, 10))
plt.scatter(x, y, color='navy', s=30, marker='o', label="dados")

for count, degree in enumerate([3 ,4, 5]):
    model = make_pipeline(PolynomialFeatures(degree), Ridge())
    model.fit(X, y)
    y_plot = model.predict(X_plot)
    plt.plot(x, y_plot, color=colors[count], linewidth=lw, label="degree %d" % degree)
    plt.locator_params(axis="x", nbins=4)

plt.legend(loc='upper left')
plt.title("Casos OpenSUS - Porto Velho")
ax.xaxis.set_major_locator(MaxNLocator(10))
ax.tick_params(axis='x', rotation=45)

plt.show()